Cubescape port of the ola-python module to work with Python 3.

Original author: nomis52@gmail.com (Simon Newton)

URLs:

- https://www.openlighting.org/
- https://www.openlighting.org/ola/developer-documentation/python-api/
- https://github.com/OpenLightingProject/ola

